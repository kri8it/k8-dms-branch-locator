## CHANGELOG

= 1.0 =
* Initial release

= 1.0.2 =
* Added GitHub Updater Support

= 1.0.3 =
* K8 Updater compatibility