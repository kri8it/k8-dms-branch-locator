<?php

/*
	Plugin Name: K8 DMS Branch Locator
	Plugin URI: https://bitbucket.org/kri8it/k8-dms-branch-locator
	Description: Add a Branch Locator to your site
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.3
	Section: true
	Class Name: K8BranchLocator
	Filter: component, dual-width
	Loading: refresh
	
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-branch-locator	
 
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

if( ! class_exists('acf') ) // Requires Advanced Custom Fields Pro
	return;
	
class K8BranchLocator extends PageLinesSection {
	
	public $lat = '37.774929';
	public $lng = '-122.419416';
	public $desc = 'You have no branches set up. Add one now - <a href="/wp-admin/post-new.php?post_type=branch">click here</a>';
	public $help = 'To find map the coordinates use this easy tool: <a target="_blank" href="http://www.mapcoordinates.net/en">ww.mapcoordinates.net</a>';
	
	function section_styles(){

		wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js?sensor=false', NULL, NULL, true );
		wp_enqueue_script( 'k8-maps', $this->base_url.'/maps.js', array( 'jquery' ), pl_get_cache_key(), true );
		
	}
	
	function section_persistent(){
        
		/**
		 * Clear Branch Cache on Update
		 */
		add_action( 'save_post', array( $this, 'refresh_branch_cache' ), 10, 3 );
		
		/**
		 * Register Branch Post Type
		 */
		 
		/* Branch Post Type -------------------------------------------------------------- */
	
		$labels = array(
			'name' => _x( 'Branches', 'post type general name', 'k8' ),
			'singular_name' => _x( 'Branch', 'post type singular name', 'k8' ),
			'add_new' => _x( 'Add New', 'branch', 'k8' ),
			'add_new_item' => __( 'Add Branch', 'k8' ),
			'edit_item' => __( 'Edit Branch', 'k8' ),
			'new_item' => __( 'New Branch', 'k8' ),
			'view_item' => __( 'View Branch', 'k8' ),
			'search_items' => __( 'Search Branches', 'k8' ),
			'not_found' =>  __( 'No Branches found', 'k8' ),
			'not_found_in_trash' => __( 'No Branches found in Trash', 'k8' ), 
			'parent_item_colon' => ''
		);
		
		$rewrite = 'branch';
		
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'query_var' => true,
			'rewrite' => array( 'slug' => $rewrite ),
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_icon' => 'dashicons-location-alt',
			'menu_position' => null, 
			'has_archive' => true, 
			'taxonomies' => array( 'region' ), 
			'supports' => array( 'title','editor','thumbnail' )
		);
				
		register_post_type( 'branch', $args );
		
		/* "Region" Custom Taxonomy ---------------------------------------------------------------- */
		$labels = array(
			'name' => _x( 'Region', 'taxonomy general name', 'k8' ),
			'singular_name' => _x( 'Region', 'taxonomy singular name','k8' ),
			'search_items' =>  __( 'Search Regions', 'k8' ),
			'all_items' => __( 'All Regions', 'k8' ),
			'parent_item' => __( 'Parent Region', 'k8' ),
			'parent_item_colon' => __( 'Parent Region:', 'k8' ),
			'edit_item' => __( 'Edit Region', 'k8' ), 
			'update_item' => __( 'Update Region', 'k8' ),
			'add_new_item' => __( 'Add New Region', 'k8' ),
			'new_item_name' => __( 'New Region Name', 'k8' ),
			'menu_name' => __( 'Regions', 'k8' )
		); 	
		
		$args = array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'query_var' => 'region',
			'show_admin_column' => true,
			'rewrite' => array( 'slug' => 'regions', 'hierarchical' => true )
		);
		
		
		register_taxonomy( 'region', array( 'branch' ), $args );
		
		
		/**
		 * Register Branch Custom Fields
		 * 
		 */
		 if( function_exists('register_field_group') ):

			register_field_group(array (
				'key' => 'group_5458b1b9dac64',
				'title' => 'Branch Details',
				'fields' => array (
					array (
						'key' => 'field_5458b272a99e1',
						'label' => 'Physical Address',
						'name' => 'physical_address',
						'prefix' => '',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => 'br',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5458b2d7a99e2',
						'label' => 'Postal Address',
						'name' => 'postal_address',
						'prefix' => '',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => 'br',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5458b32ba99e3',
						'label' => 'Contact Number',
						'name' => 'contact_number',
						'prefix' => '',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5458b5cea99e4',
						'label' => 'Fax Number',
						'name' => 'fax_number',
						'prefix' => '',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5458b5d9a99e5',
						'label' => 'Email Contact',
						'name' => 'email_contact',
						'prefix' => '',
						'type' => 'email',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
					),
					array (
						'key' => 'field_5458b5fca99e6',
						'label' => 'Branch Location',
						'name' => 'branch_location',
						'prefix' => '',
						'type' => 'google_map',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'center_lat' => '',
						'center_lng' => '',
						'zoom' => '',
						'height' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'branch',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				));
			
			endif;		
		
    }

	/**
	 * Prime the cache for the branch posts.
	 *
	 * @param int $post_id 	Post ID.
	 * @param int $post     The post.
	 * .
	 */
	protected function refresh_branch_cache( $post_id, $post, $update ) {
	    
		//Check for branch post type
		$slug = 'branch';
		
		if ( $slug != $post->post_type ) return;
							
	    // Force the cache refresh for branch posts.
	    $this->get_branches( $force_refresh = true );
		
	}	
	
	/**
	 * Retreive Branch Locations and create cache
	 * 
	 * @param bool $force_refresh Optional. Whether to force the cache to be refreshed.
	 * 
	 * @return array|WP_Error Array of WP_Post objects
	 * 
	 * @since 1.0
	 */
	protected function get_branches( $force_refresh = false ){
		
		// Check for the branches key in the 'k8_branch_locator' group.
    	$branches = wp_cache_get( 'k8_branch_locator_branches', 'k8_branch_locator' );
		
		// If nothing is found, build the object.
	    if ( true === $force_refresh || false === $branches ) {
	        	
	        $args = array(
				'post_type' => 'branch',
				'posts_per_page' => 500,
				'orderby' => 'title',
				'no_found_rows' => true
			);		
			$branches = new WP_Query( $args );
	
	        if ( ! is_wp_error( $branches ) && $branches->have_posts() ) {
	            	
	            // In this case we don't need a timed cache expiration.
	            wp_cache_set( 'k8_branch_locator_branches', $branches, 'k8_branch_locator' );
				
	        }
	    }
						
		return $branches;
		
	}
	
	function section_head() {
		
		global $post;
		
		$marker_image = $this->opt( 'k8_branch_locator_marker_image' );
							
		if( is_singular( 'branch' ) ):		
			$locations = array( $post );
		else:		
			$locations = $this->get_branches()->posts;
		endif;
			
		$maps = array();
		$defaults = array(
			'lat'		=> floatval( $this->lat ),
			'lng'		=> floatval( $this->lng ),
			'mapinfo'	=> $this->desc,
			'image'		=> $this->base_url.'/marker.png'
		);

		if( ! is_array( $locations ) ) {
			$maps = array(
					1 => $defaults
			);
		} else {
			$maps = array();
			$i = 1;
			foreach( $locations as $k => $location ) {
				
				$title			= get_the_title( $location->ID );
				$data 			= get_field( 'branch_location', $location->ID );
				$contact_number = get_field( 'contact_number', $location->ID );
				$fax_number 	= get_field( 'fax_number', $location->ID );
				$email 			= get_field( 'email_contact', $location->ID );
				$link 			= get_permalink( $location->ID );
				
				$mapinfo = '<h5>' . $title . '</h5>';
				$mapinfo .= ( $contact_number ? '<b>T:</b>' . $contact_number . '<br/>' : '' ); 
				$mapinfo .= ( $fax_number ? '<b>F:</b>' . $fax_number . '<br/>' : '' ); 
				$mapinfo .= ( $email ? '<b>E:</b><a href="mailto:' . $email . '">' . $email . '</a><br/>' : '' ); 
				if( !is_singular( 'branch' ) ) $mapinfo .= sprintf( '<a class="btn btn-small btn-link-color" href="%s">%s</a>', $link, __( 'Click Here', 'pagelines' ) );
				
				$maps[$i] = array(
					'lat'		=> ( isset( $data['lat'] ) ) ? floatval( $data['lat'] ): $this->lat,
					'lng'		=> ( isset( $data['lng'] ) ) ? floatval( $data['lng'] ) : $this->lng,
					'mapinfo'	=> $mapinfo,
					'image'		=> ( isset( $marker_image ) && '' != $marker_image ) ? do_shortcode( $marker_image ) : $this->base_url.'/marker.png'
				);
				
				if( $i == 1 ){
					$this->lat = $data['lat'];
					$this->lng = $data['lng'];
				}
				
				$i++;
			}
		}
						
		$main = array(
			'lat'			=> $this->opt( 'k8_branch_locator_center_lat', array( 'default' => $this->lat ) ),
			'lng'			=> $this->opt( 'k8_branch_locator_center_lng', array( 'default' => $this->lng ) ),
			'zoom_level'	=> floatval( $this->opt( 'k8_branch_locator_map_zoom_level', array( 'default' => 10 ) ) ),
			'zoom_enable'	=> $this->opt( 'k8_branch_locator_map_zoom_enable', array( 'default' => true ) ),
			'enable_animation' => $this->opt( 'k8_branch_locator_enable_animation', array( 'default' => true ) ),
			'image'			=> $this->base_url.'/marker.png'
		);

		wp_localize_script( 'k8-maps', 'map_data_' . $this->meta['unique'], $maps );

		wp_localize_script( 'k8-maps', 'map_main_' . $this->meta['unique'], $main );
		
	}
			
	function section_template() {
		
		global $post;
		
		$default_title = ( is_singular( 'branch' ) ? $post->post_title : __( 'Find a Branch Near You', 'k8' ) );
		
		$title = $this->opt( 'k8_branch_locator_title', array( 'default' => $default_title ) );
		$title_wrap = $this->opt( 'k8_branch_locator_title_wrap', array( 'default' => 'h2' )  );
		$desc = $this->opt( 'k8_branch_locator_desc' );
		
		printf( '<%s class="form-head" data-sync="k8_gform_title">%s</%s>', $title_wrap, $title, $title_wrap );
		
		if( $desc != '' ) echo '<p class="k8-branch-locator-desc">' . $desc . '</p>';
		
		if( ! $this->opt( 'k8_branch_locator_disable_dropdown' ) ){
						
			$locations = $this->get_branches();
			
			if( is_array( $locations->posts ) && !empty( $locations->posts ) ){
				
				echo '<select name="k8-branch-locator-select" id="k8-branch-locator-select" class="k8-branch-locator-select" onchange="if( jQuery(this).val() != -1 ) window.location.href = jQuery(this).val();">';
					
					echo '<option value="-1">' . __( 'Select a Branch', 'k8' ) . '</option>';
					
				foreach( $locations->posts as $location ){
					echo '<option value="' . get_permalink( $location->ID ) . '">' . $location->post_title . '</option>';
				}
				
				echo '</select>';
				
			}
			
		}
		
		$height = $this->opt( 'k8_branch_locator_map_height', array( 'default' => '350px' ) );	
		printf( '<div class="k8_branch_locator-map-wrap pl-animation pl-slidedown"><div id="k8_branch_locator_map_%s" data-map-id="%s" class="k8_branch_locator-map pl-end-height" style="height: %s"></div></div>', $this->meta['unique'], $this->meta['unique'], $height );		
		
	}

	function section_opts(){
						
		$options = array();
		
		$options[] = array(
				'type'	=> 'multi',
				'key'	=> 'k8_branch_locator_settings',
				'title'	=> __( 'Branch Locator Settings', 'pagelines' ),
				'col'	=> 1,
				'opts'	=> array(

					array(
						'key'	=> 'k8_branch_locator_title',
						'type'	=> 'text',
						'default'	=> __( 'Find a Branch Near You', 'pagelines' ),
						'place'		=> __( 'Find a Branch Near You', 'pagelines' ),
						'label'	=> __( 'Title', 'pagelines' )						
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_branch_locator_title_wrap',
						'label' 		=> __( 'Title Text Wrapper', 'pagelines' ),
						'default'		=> 'h2',
						'opts'			=> array(
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;  (default)'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
						)
					),
					array(
						'key'	=> 'k8_branch_locator_desc',
						'type'	=> 'textarea',						
						'label'	=> __( 'Description', 'pagelines' )						
					),					
					array(
						'type'	=> 'check',
						'key'	=> 'k8_branch_locator_disable_dropdown',
						'label'	=> __( 'Disable Branch Dropdown', 'pagelines' ),
						'default'		=> false,
						'compile'		=> true,
					),
				)

			);
		
		$options[] = array(
				'type'	=> 'multi',
				'key'	=> 'k8_branch_map_config',
				'title'	=> __( 'Google Maps Configuration', 'pagelines' ),
				'col'	=> 2,
				'opts'	=> array(

					array(
						'key'	=> 'k8_branch_locator_center_lat',
						'type'	=> 'text_small',
						'default'	=> $this->lat,
						'place'		=> $this->lat,
						'label'	=> __( 'Latitude', 'pagelines' ),
						'help'	=> $this->help
					),
					array(
						'key'	=> 'k8_branch_locator_center_lng',
						'type'	=> 'text_small',
						'default'	=> $this->lng,
						'place'	=> $this->lng,
						'label'	=> __( 'Longitude', 'pagelines' ),
						'help'	=> $this->help
					),
					
					array(
						'key'		=> 'k8_branch_locator_marker_image',
						'label' 	=> __( 'Pointer Image', 'pagelines' ),
						'type'		=> 'image_upload',
						'help'		=> __( 'For best results use an image size of 64 x 64 pixels.', 'pagelines' )
					),
					
					array(
						'type'	=> 'select',
						'key'	=> 'k8_branch_locator_map_height',
						'default'	=> '350px',
						'label'	=> __( 'Select Map Height ( default 350px)', 'pagelines' ),
						'opts'	=> array(
							'200px'	=> array( 'name' => '200px'),
							'250px'	=> array( 'name' => '250px'),
							'300px'	=> array( 'name' => '300px'),
							'350px'	=> array( 'name' => '350px'),
							'400px'	=> array( 'name' => '400px'),
							'450px'	=> array( 'name' => '450px'),
							'500px'	=> array( 'name' => '500px'),
						)
					),
					array(
						'type'	=> 'count_select',
						'key'	=> 'k8_branch_locator_map_zoom_level',
						'default'	=> '12',
						'label'	=> __( 'Select Map Zoom Level ( default 10)', 'pagelines' ),
						'count_start'	=> 1,
						'count_number'	=> 18,
						'default'		=> '10',
					),
					array(
						'type'	=> 'check',
						'key'	=> 'k8_branch_locator_map_zoom_enable',
						'label'	=> __( 'Enable Zoom Controls', 'pagelines' ),
						'default'		=> true,
						'compile'		=> true,
					),
					array(
						'type'	=> 'check',
						'key'	=> 'k8_branch_locator_enable_animation',
						'label'	=> __( 'Enable Animations', 'pagelines' ),
						'default'		=> true,
						'compile'		=> true,
					),
				)

			);

		
		return $options;
	}
}